const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json())
// 處理請求類型 
/* express4 已淘汰body-parser, cookie-parser  */
app.use(bodyParser.urlencoded({extended: true}));

// 測試
app.get('/', (req,res) =>{
  res.json({message:"Welcome to this application."})
})

require("./app/routes/customer.routes.js")(app);


// 設定port
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
