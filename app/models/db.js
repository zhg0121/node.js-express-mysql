// const mysql = require("mysql");
// const dbConfig = require("../config/db.config.js");

// // 建立連線
// const connection = mysql.createConnection({
//     host: dbConfig.HOST,
//     user: dbConfig.USER,
//     password: dbConfig.PASSWORD,
//     database: dbConfig.DB,
// });

// // 啟動連線
// connection.connect(error => {
//   if(error) throw error;
//   console.log("Successfully connected to the database.")
// })

// module.exports = connection;

const mysql = require("mysql");
const dbConfig = require("../config/db.config.js");

var connection = mysql.createPool({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
});

module.exports = connection;